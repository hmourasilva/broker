package br.com.primetech.broker.util;

import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Duration;

public class BrokerDateUtils {

	/**
	 * @param initialDt
	 * @param finalDt
	 * @return
	 */
	public static long subtrairDatas(Date initialDt, Date finalDt) {
		long result = 0;
		try {
			// ---------------------------------------------------
			Calendar cal = Calendar.getInstance();
			cal.setTime(initialDt);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			// ---------------------------------------------------
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(finalDt);
			cal2.set(Calendar.HOUR_OF_DAY, 0);
			cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
			// ---------------------------------------------------

			DateTime dt = new DateTime(cal.getTime());
			DateTime dt2 = new DateTime(cal2.getTime());

			Duration dur = new Duration(dt, dt2);
			result = dur.getStandardDays();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
