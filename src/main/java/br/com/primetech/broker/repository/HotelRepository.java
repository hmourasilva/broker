package br.com.primetech.broker.repository;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.primetech.broker.model.Hotel;

@FeignClient(url="https://cvcbackendhotel.herokuapp.com/hotels", name = "hotel")
public interface HotelRepository {

	@GetMapping("/avail/{cityCode}?formato=json")
	public List<Hotel> find(@PathVariable("cityCode") Long cityCode);
	
	@GetMapping("/{hotelId}?formato=json")
	public List<Hotel> findById(@PathVariable("hotelId") Long hotelId);
}
