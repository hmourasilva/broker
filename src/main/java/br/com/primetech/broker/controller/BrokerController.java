package br.com.primetech.broker.controller;

import java.io.Serializable;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.primetech.broker.service.BrokerService;

@RestController
@RequestMapping(value = "/v1/broker")
public class BrokerController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private BrokerService service;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> find(
			@RequestParam("cityCode") Long cityCode,
			@RequestParam("checkin") @DateTimeFormat(pattern = "dd/MM/yyyy") Date checkin,
			@RequestParam("checkout") @DateTimeFormat(pattern = "dd/MM/yyyy") Date checkout,
			@RequestParam("adults") Long adults,
			@RequestParam("childs") Long childs) throws Exception {		
		return new ResponseEntity<>(service.calculate(cityCode, checkin, checkout, adults, childs), HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> findById(
			@PathVariable(name = "id") Long id,
			@RequestParam("cityCode") Long cityCode,
			@RequestParam("checkin") @DateTimeFormat(pattern = "dd/MM/yyyy") Date checkin,
			@RequestParam("checkout") @DateTimeFormat(pattern = "dd/MM/yyyy") Date checkout,
			@RequestParam("adults") Long adults,
			@RequestParam("childs") Long childs) throws Exception {		
		return new ResponseEntity<>(service.calculateDetail(id, cityCode, checkin, checkout, adults, childs), HttpStatus.ACCEPTED);
	}
}
