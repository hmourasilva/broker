package br.com.primetech.broker;

import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
@EnableFeignClients
public class BrokerApplication {

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(BrokerApplication.class);
        app.setBannerMode(Mode.OFF);
        app.run(args);
	}
}
