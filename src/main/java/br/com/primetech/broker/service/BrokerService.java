package br.com.primetech.broker.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.primetech.broker.mapper.HotelMapper;
import br.com.primetech.broker.mapper.HotelMapperImpl;
import br.com.primetech.broker.model.Hotel;
import br.com.primetech.broker.repository.HotelRepository;
import br.com.primetech.broker.resource.HoterResource;
import br.com.primetech.broker.resource.RoomResource;
import br.com.primetech.broker.util.BrokerDateUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BrokerService {

	@Autowired
	private HotelRepository repository;
	
	private HotelMapper mapper = new HotelMapperImpl();
	
	/**
	 * @param cityCode
	 * @return
	 */
	public List<Hotel> find(Long cityCode){
		return this.repository.find(cityCode);
	}
	
	/**
	 * @param hotelId
	 * @return
	 */
	public List<Hotel> findById(Long hotelId){
		return this.repository.findById(hotelId);
	}
	
	/**
	 * @param cityCode
	 * @param checkin
	 * @param checkout
	 * @param adults
	 * @param childs
	 * @return
	 */
	public List<HoterResource> calculate(Long cityCode,
			Date checkin,
			Date checkout,
			Long adults,
			Long childs) {		
		return this.iterateHotels(this.find(cityCode), checkin, checkout, adults, childs);
	}
	
	public List<HoterResource> calculateDetail(Long hotelId,
			Long cityCode,
			Date checkin,
			Date checkout,
			Long adults,
			Long childs) {		
		return this.iterateHotels( this.findById(hotelId), checkin, checkout, adults, childs);
	}
	
	/**
	 * @param availHotels
	 * @param checkin
	 * @param checkout
	 * @param adults
	 * @param childs
	 * @return
	 */
	public List<HoterResource> iterateHotels(List<Hotel> availHotels, 
			Date checkin,
			Date checkout,
			Long adults,
			Long childs) {		
		List<HoterResource> resources = new ArrayList<>();		
		availHotels.forEach(h -> resources.add(processHotel(h, BrokerDateUtils.subtrairDatas(checkin, checkout), adults, childs)));		
		return resources;		
	}
	
	/**
	 * @param hotel
	 * @param totalDays
	 * @param adults
	 * @param childs
	 * @return
	 */
	public HoterResource processHotel(Hotel hotel,
			long totalDays,
			Long adults,
			Long childs) {				
		HoterResource resource = mapper.hotelToHotelResource(hotel);
		this.calculateTotalPrice(resource.getRooms(), totalDays, adults, childs);
		return resource;
	}
	
	/**
	 * @param rooms
	 * @param totalDays
	 * @param adults
	 * @param childs
	 */
	public void calculateTotalPrice(List<RoomResource> rooms,
			long totalDays,
			Long adults,
			Long childs) {		
		rooms.forEach(room -> this.calculate(room, totalDays, adults, childs));
	}
	
	/**
	 * @param room
	 * @param totalDays
	 * @param adults
	 * @param childs
	 */
	private void calculate(RoomResource room,
			long totalDays,
			Long adults,
			Long childs) {		
		try {			
			BigDecimal totalDaysNumber = new BigDecimal(totalDays).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			
			BigDecimal pricePerDayAdult = room.getPriceDetail().getPricePerDayAdult().setScale(2, BigDecimal.ROUND_HALF_EVEN)
					.multiply(totalDaysNumber)
					.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			
			BigDecimal pricePerDayChild = room.getPriceDetail().getPricePerDayChild().setScale(2, BigDecimal.ROUND_HALF_EVEN)
					.multiply(totalDaysNumber)
					.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			
			BigDecimal commission = new BigDecimal(0.7D).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BigDecimal adultsNumber = new BigDecimal(adults).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			BigDecimal childsNumber = new BigDecimal(childs).setScale(2, BigDecimal.ROUND_HALF_EVEN);
			
			BigDecimal commissionAdult = pricePerDayAdult.divide(commission,2, BigDecimal.ROUND_HALF_EVEN);
			BigDecimal commissionChild = pricePerDayChild.divide(commission,2, BigDecimal.ROUND_HALF_EVEN);
			BigDecimal totalPriceAdult = sumTotalPrice(pricePerDayAdult, adultsNumber, commissionAdult);
			BigDecimal totalPriceChild = sumTotalPrice(pricePerDayChild, childsNumber, commissionChild);
			
			room.setTotalPrice(totalPriceAdult.add(totalPriceChild).setScale(2, BigDecimal.ROUND_HALF_EVEN));
			room.getPriceDetail().setPricePerDayAdult(pricePerDayAdult);
			room.getPriceDetail().setPricePerDayChild(pricePerDayChild);
			
		} catch (Exception e) {
			log.error("FALHA AO CALCULAR 'TotalPrice'",e);
		}	
	}

	/**
	 * @param pricePerDay
	 * @param gestNumber
	 * @param commission
	 * @return
	 */
	private BigDecimal sumTotalPrice(BigDecimal pricePerDay, BigDecimal gestNumber, BigDecimal commission) {
		if(gestNumber.longValue() == 0) {
			return BigDecimal.ZERO;
		}
		return pricePerDay.multiply(gestNumber).add(commission).setScale(2, BigDecimal.ROUND_HALF_EVEN);
	}
}
