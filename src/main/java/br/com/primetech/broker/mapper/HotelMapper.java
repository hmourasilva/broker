package br.com.primetech.broker.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import br.com.primetech.broker.model.Hotel;
import br.com.primetech.broker.model.Price;
import br.com.primetech.broker.model.Room;
import br.com.primetech.broker.resource.HoterResource;
import br.com.primetech.broker.resource.PriceResource;
import br.com.primetech.broker.resource.RoomResource;

@Mapper
public interface HotelMapper {

	
	HoterResource hotelToHotelResource(Hotel hotel);
	
	List<HoterResource> hotelToHotelResource(List<Hotel> hotel);
	
	@Mapping(source = "price", target = "priceDetail")
	RoomResource roomToRoomResource(Room rooms);
	
	List<RoomResource> roomToRoomResource(List<Room> rooms);
		
	@Mapping(source = "adult", target = "pricePerDayAdult")
	@Mapping(source = "child", target = "pricePerDayChild")	
	PriceResource priceToPriceResource(Price price);	
}
