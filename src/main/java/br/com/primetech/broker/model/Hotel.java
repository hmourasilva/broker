package br.com.primetech.broker.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Hotel {

	private Long id;
	private String name;
	private Long cityCode;
	private String cityName;
	private List<Room> rooms;
}
