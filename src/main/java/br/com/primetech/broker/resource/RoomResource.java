package br.com.primetech.broker.resource;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RoomResource implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long roomID;
	private String categoryName;
	private BigDecimal totalPrice;
	private PriceResource priceDetail;	
}
